package edu.hawaii.ics211;


public class TestCalculator {
	public static int correct = 0;
	public static int incorrect = 0;
	public static boolean test(int expected, int actual)
	{
		if(expected == actual)
		{
			System.out.println(" The Test passed the expected value "+expected+" is equal to the actual value "+actual);
			correct++;
			return true;
		}
		else if(expected != actual){
			System.err.println('\n'+" The Test Failed the expected value "+expected+" is not equal to the actual value "+actual);
			incorrect++;
			return false;
		}
		return false;
	}
	public static void main(String args[]) {
		//Create the calculator 
		Calculator example = new Calculator();
		//Create the variable to hold the result and initialize it to 0
		int result = 0;
		// Add test
		System.out.println(" Test The Add Method: ");
		// positive and positive
		result = example.add(4,5);
		test(9,result);
		// positive and negative
		result = example.add(-4,5);
		test(1,result);
		// negative and negative
		result = example.add(-4,-5);
		test(-9,result);
		// Divide test
		System.out.println(" Test The Divide Method: ");
		//bigger numerator
		result = example.divide(8,2);
		test(4,result);
		//bigger denominator
		
		result = example.divide(2,8);
		
		//0 denominator
		try{
		result = example.divide(4,0);
		System.out.println(" if this line is executed code fails ");
		}
		catch(ArithmeticException e)
		{
			System.out.println("divide by 0 test passed");
		}
		//0 numerator
		result = example.divide(0,4);
		test(0,result);
		//1 numerator 
		result = example.divide(1,5);
		System.out.println(" Note: since all of the methods we are testing return an int methods such as divide will be expected to drope the trailing decimal places");
		test(0,result);
		//1 denominator
		result = example.divide(5,1);
		test(5,result);
		// negative numerator
		result = example.divide(-8,2);
		test(-4,result);
		//negative denominator
		result = example.divide(8,-1);
		test(-8,result);
		// Modulo test
		System.out.println(" Test The Modulo Method: ");
		// two of the same numbers
		result = example.modulo(2,2);
		test(0,result);
		// number with a difference
		result = example.modulo(5,2);
		test(1,result);
		//other way arround
		result = example.modulo(2, 3);
		test(1,result);
		
		// Multiply test
		System.out.println(" Test The Multiply Method: ");
		// two positives
		result = example.multiply(2, 3);
		test(6,result);
		//two negatives
		result = example.multiply(-2, -3);
		test(6,result);
		//one negative one positive
		result = example.multiply(2,-3);
		test(-6,result);
		// 0  
		result = example.multiply(2,0);
		test(0,result);
		//1
		result = example.multiply(2,1);
		test(2,result);
		// Pow test
		System.out.println(" Test The Pow Method: ");
		System.out.println(" Note: fractional powers arent conscidered scincce the method takes only two integers as parrameters and returns an int.");
		// to the first power
		result = example.pow(6,1);
		test(6,result);
		// to an odd power
		result = example.pow(2,3);
		test(8,result);
		// to an even power
		result = example.pow(2,2);
		test(4,result);
		// to the 0 power
		result = example.pow(6,0);
		test(1,result);
		//to a negative power sqrt
		result = example.pow(2,-1);
		test(-8,result);
		// base 1
		result = example.pow(1, 300);
		test(1,result);
		//base 0
		result = example.pow(0, 350);
		test(0,result);
		
	
		// Subtract test
		System.out.println(" Test The Subtract Method: ");
		result = example.subtract(4,5);
		test(-1,result);
		// positive and negative
		result = example.subtract(-4,5);
		test(-9,result);
		// negative and negative
		result = example.subtract(-4,-5);
		// Divide test
		test(1,result);
	
		System.out.println(" There were "+correct+" functioning and "+incorrect+" failed methods out of  "+(correct+incorrect)+"  total methods tested today");
		
		
	}

}
